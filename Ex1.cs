﻿using NHibernate;
using NHibernate.Cfg;
using System;
using System.Reflection;

class Ex1
{
    static void Main(string[] args)
    {
        Configuration configuration = new Configuration();
        configuration.AddAssembly(Assembly.GetCallingAssembly());
        ISessionFactory sessionFactory = configuration.BuildSessionFactory();
        ISession session = sessionFactory.OpenSession();

        Ex1 ex1 = new Ex1();

        double HqlTime = ex1.MeasureExecutionTime(session, 75000, ex1.HqlMethod);
        double ICriteriaTime = ex1.MeasureExecutionTime(session, 75000, ex1.ICriteriaMethod);
        Console.WriteLine("Hql speed 75000 rows: " + HqlTime);
        Console.WriteLine("ICriteria speed 75000 rows: " + ICriteriaTime);

        HqlTime = ex1.MeasureExecutionTime(session, 50000, ex1.HqlMethod);
        ICriteriaTime = ex1.MeasureExecutionTime(session, 50000, ex1.ICriteriaMethod);
        Console.WriteLine("Hql speed 50000 rows: " + HqlTime);
        Console.WriteLine("ICriteria speed 50000 rows: " + ICriteriaTime);

        HqlTime = ex1.MeasureExecutionTime(session, 25000, ex1.HqlMethod);
        ICriteriaTime = ex1.MeasureExecutionTime(session, 25000, ex1.ICriteriaMethod);
        Console.WriteLine("Hql speed 25000 rows: " + HqlTime);
        Console.WriteLine("ICriteria speed 25000 rows: " + ICriteriaTime);

        HqlTime = ex1.MeasureExecutionTime(session, 1000, ex1.HqlMethod);
        ICriteriaTime = ex1.MeasureExecutionTime(session, 1000, ex1.ICriteriaMethod);
        Console.WriteLine("Hql speed 1000 rows: " + HqlTime);
        Console.WriteLine("ICriteria speed 1000 rows: " + ICriteriaTime);
            
        Console.ReadKey();

    }

    private double MeasureExecutionTime(ISession session, int numRows, Action<ISession, int> Method)
    {
        double time = 0;
        for (int j = 0; j < 10; j++)
        {
            for (int i = 0; i < 10; i++)
            {
                var watch = new System.Diagnostics.Stopwatch();
                watch.Start();
                Method(session, numRows);
                watch.Stop();
                if (i == 5 || i == 6)
                {
                    time += watch.ElapsedMilliseconds;
                }
            }
        }
        return time / 2;
    }

    private void HqlMethod(ISession session, int numRows)
    {
        String hql = "FROM SampleTable;";
        IQuery query = session.CreateQuery(hql);
        query.SetMaxResults(numRows).List();
    }

    private void ICriteriaMethod(ISession session, int numRows)
    {
        ICriteria criteria = session.CreateCriteria<SampleTable>();
        criteria.SetMaxResults(numRows).List();
    }
}

class SampleTable
{
    public virtual string RetailerCountry { get; set; }
    public virtual string OrderMethodType { get; set; }
    public virtual string RetailerType { get; set; }
    public virtual string ProductLine { get; set; }
    public virtual string ProductType { get; set; }
    public virtual string Product { get; set; }
    public virtual int Year { get; set; }
    public virtual string Quarter { get; set; }
    public virtual float Revenue { get; set; }
    public virtual int Quantity { get; set; }
    public virtual float GrossMargin { get; set; }
}